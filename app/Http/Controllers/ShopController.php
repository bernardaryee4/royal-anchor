<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('products')
            ->join('categories', 'products.categoryId', 'categories.id')
            ->inRandomOrder()
            ->get();

        return view('shop')->with('products', $products);
    }


    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($category,$slug){


        $product = DB::table('products')
            ->join('categories', 'products.categoryId', 'categories.id')
            ->where('products.slug',$slug)
            ->first();


        $relatedProducts = DB::table('products')
            ->join('categories', 'products.categoryId', 'categories.id')
            ->where('products.categoryId',$category)
            ->where('products.slug','!=',$slug)
            ->take(8)
            ->get();


        return view('product')->with('product',$product)->with('relatedProducts',$relatedProducts);
    }

}
