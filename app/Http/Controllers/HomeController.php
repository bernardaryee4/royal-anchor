<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {

        $tvs = DB::table('products')
            ->join('categories', 'products.categoryId', 'categories.id')
            ->where('products.categoryId', 1)
            ->inRandomOrder()
            ->take(6)
            ->get();
        $fridges = DB::table('products')
            ->join('categories', 'products.categoryId', 'categories.id')
            ->where('products.categoryId', 2)
            ->inRandomOrder()
            ->take(2)
            ->get();



        return view('landing-page')->with('tvs', $tvs)->with('fridges', $fridges);
    }
}
