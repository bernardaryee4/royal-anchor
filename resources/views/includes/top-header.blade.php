<!-- TOP HEADER -->
<div id="top-header">
    <div class="container">
        <ul class="header-links pull-left">
            <li><a href="#"><i class="fa fa-phone"></i>+233 541 859113</a></li>
            <li><a href="#"><i class="fa fa-envelope-o"></i>afeti.eugene@gmail.com</a></li>
            <li><a href="#"><i class="fa fa-map-marker"></i>No. 15 Dzorwulu</a></li>
        </ul>
        <ul class="header-links pull-right">
            <li><a href="#"><i class="fa fa-user-o"></i> My Account</a></li>
        </ul>
    </div>
</div>
<!-- /TOP HEADER -->
