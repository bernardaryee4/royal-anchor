@extends('layouts.app')


@section('content')
    @include('includes.page-header')


    <!-- NAVIGATION -->
    <nav id="navigation">
    <!-- container -->
        <div class="container">
        <!-- responsive-nav -->
            <div id="responsive-nav">
            <!-- NAV -->
                <ul class="main-nav nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">Hot Deals</a></li>
                    <li><a href="#">Categories</a></li>
                    <li><a href="#">Laptops</a></li>
                    <li><a href="#">Smartphones</a></li>
                    <li><a href="#">Cameras</a></li>
                    <li><a href="#">Accessories</a></li>
                </ul>
            <!-- /NAV -->
            </div>
        <!-- /responsive-nav -->
        </div>
    <!-- /container -->
    </nav>
    <!-- /NAVIGATION -->

    <!-- BREADCRUMB -->
    <div id="breadcrumb" class="section">
    <!-- container -->
        <div class="container">

            @if(session()->has('success_message'))
                <div class="alert alert-success">
                    {{session()->get('success_message')}}
                </div>
            @endif

            @if(count($errors) > 0))
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <!-- row -->
				<div class="row">
					<div class="col-md-12">
						<h3 class="breadcrumb-header">Your Shopping Cart</h3>
						<ul class="breadcrumb-tree">
							<li><a href="#">Home</a></li>
							<li class="active">Blank</li>
						</ul>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /BREADCRUMB -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
                @if(Cart::count() > 0)
				<div class="row">
                    <h6>You have {{Cart::count()}} item(s) in your cart</h6>

                    <div class="cart-list">
                        @foreach(Cart::content() as $item)

                             <div class="product-widget">
                                    <div class="product-img">
                                        <img src={{asset('img/'.$item->model->image)}} alt="">
                                    </div>
                                    <div class="product-body">
                                       <p>
                                        <h3 class="product-name">
                                            <a href="{{route('shop.show',[$item->categoryId,$item->slug])}}">{{$item->name}}</a>
                                        </h3>
                                        <form action="{{route('cart.destroy', $item->rowId)}}" method="POST">
                                            {{csrf_field()}}
                                            {{method_field("DELETE")}}
                                            <button type="submit" class="delete-item-from-cart btn btn-danger btn-sm">x</button>
                                        </form>
                                        </p>
                                        <h4 class="product-price"><span class="qty">1x</span>&#8373;{{$item->price}}</h4>

                                    </div>

                                </div>

                        @endforeach
                    </div>

                    <h4 class="product-price">&#8373;{{Cart::subtotal()}}</h4>
                    <h4 class="product-price">&#8373;{{Cart::tax()}}</h4>
                    <h4 class="product-price">&#8373;{{Cart::total()}}</h4>

                </div>
                @else
                    <div class="text-center row">
                        <h3 class="">You have no items in your cart </h3>
                        <a class="btn add-to-cart-btn text-center" href="{{route('shop.index')}}">Continue Shopping</a>
                    </div>
                @endif
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- NEWSLETTER -->
		<div id="newsletter" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="newsletter">
							<p>Sign Up for the <strong>NEWSLETTER</strong></p>
							<form>
								<input class="input" type="email" placeholder="Enter Your Email">
								<button class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribe</button>
							</form>
							<ul class="newsletter-follow">
								<li>
									<a href="#"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-instagram"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-pinterest"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /NEWSLETTER -->

	@include('includes.footer')

    <!-- jQuery Plugins -->
    <script src={{asset('js/jquery.min.js')}}></script>
    <script src={{asset('js/bootstrap.min.js')}}></script>
    <script src={{asset('js/slick.min.js')}}></script>
    <script src={{asset('js/nouislider.min.js')}}></script>
    <script src={{asset('js/jquery.zoom.min.js')}}></script>
    <script src={{asset('js/main.js')}}></script>
@endsection
