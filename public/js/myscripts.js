$(document).ready(function(){
    setTimeout(function(){
        $('body').addClass('loaded');
        $('h1').css('color','#222222');
    }, 1000);



    $(".dropdown, .btn-group").hover(function(){
        var dropdownMenu = $(this).children(".dropdown-menu");
        if(dropdownMenu.is(":visible")){
            dropdownMenu.parent().toggleClass("open");
        }
    });


    var deadline = "2019-07-20";
    if(deadline != null){
        initializeClock('date', deadline);
    }

    function initializeClock(id, deadline){
        updateClock();

        var timeinterval = setInterval(updateClock,1000);

        function updateClock(){
            var t = getTimeRemaining(deadline);
            $(".days").text(t.days);
            $(".hours").text(t.hours);
            $(".minutes").text(t.minutes);
            $(".seconds").text(('0' + t.seconds).slice(-2));

            if(t.total<=0){
                clearInterval(timeinterval);
            }
        }


        function getTimeRemaining(deadline){

            let diff = Date.parse(deadline) - Date.parse(new Date());
            console.log(diff);
            let seconds = Math.floor((diff / 1000) % 60);
            let minutes = Math.floor((diff / 1000 / 60) % 60);
            let hours = Math.floor((diff / (1000 * 60 * 60)) % 24);
            let days = Math.floor(diff / (1000 * 60 * 60 * 24));


            return {
                'difference':diff,
                'days':days,
                'hours':hours,
                'minutes':minutes,
                'seconds':seconds
            }
        }
    }




});
