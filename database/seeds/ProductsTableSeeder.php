<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name'=>"Lloyd 64-inch smart TV ",
            'slug'=>"lloyd-l2-1",
            'details'=>"Latest Lloyd L43FYK",
            'price'=> 42990,
            'categoryId'=>1,
            'description'=> "Higher quality screens with fewer imperfections. Presenting the Grade \"A\" 
            LEDs that have a command over sight and give you less reasons to complain about.",
            'image'=>'lloyd.png'
        ]);

        Product::create([
            'name'=>"Lloyd 58-inch smart TV",
            'slug'=>"lloyd-l3-1",
            'categoryId'=>1,
            'details'=>"Latest Lloyd GL24H0B0CF2",
            'price'=> 4160.20,
            'description'=> "Get more room for viewing your screen with wider angle display. In display technology, 
            viewing angle is the maximum angle at which a display can be viewed with acceptable visual.",
            'image'=>'tcl.png'
        ]);
        Product::create([
            'name'=>"4K Ultra HD TV",
            'slug'=>"4k-ultra-hd",
            'details'=>"Latest 4K ultra HD tech",
            'price'=> 3880.20,
            'categoryId'=>1,
            'description'=> "High Dynamic Range (HDR) video is a quantum leap forward in 4K Ultra HD picture quality 
            made possible by a new end-to-end process of creating, distributing and displaying video content",
            'image'=>'tv.png'
        ]);
        Product::create([
            'name'=>"Sclance HD TV",
            'slug'=>"brand-new-sclance-4",
            'categoryId'=>1,
            'details'=>"DMCA Skylight Sclance HD QLED Tv",
            'price'=> 4900.20,
            'description'=> "Another description and I'm out of words",
            'image'=>'tv1.png'
        ]);
        Product::create([
            'name'=>"Samsung Super HD ",
            'slug'=>"samsung-super-hd-5",
            'details'=>"Samsung HD Curved",
            'price'=> 5920.20,
            'categoryId'=>1,
            'description'=> "Super HD curved TV from the latest samsung brands with screen filter and 3D view simulation",
            'image'=>'tv3.png'
        ]);
        Product::create([
            'name'=>"Samsung QLED SmartTV",
            'slug'=>"samsung-qled-6",
            'details'=>"4th Generation Samsung Smart TV",
            'price'=> 6000.20,
            'categoryId'=>1,
            'description'=> "I'm out of product descriptions... Thank you for understanding ",
            'image'=>'tv4.png'
        ]);




        Product::create([
            'name'=>"Home Fridge",
            'slug'=>"fridge-1",
            'details'=>"Latest YMK Fridge",
            'price'=> 22990,
            'categoryId'=>2,
            'description'=> "New fridges at affordable prices",
            'image'=>'fridge.png'
        ]);

        Product::create([
            'name'=>"Double fridge",
            'slug'=>"dbl-1",
            'details'=>"Quality home appliance",
            'price'=> 2160.20,
            'categoryId'=>2,
            'description'=> "Amazing quality from home appliances",
            'image'=>'fridge-1.png'
        ]);

    }
}
