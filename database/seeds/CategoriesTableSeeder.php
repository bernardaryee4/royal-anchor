<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'categoryName'=>"Television Sets",
            'categorySlug'=>"category-1",
        ]);

        Category::create([
            'categoryName'=>"Fridges",
            'categorySlug'=>"category-2",
        ]);
    }
}
